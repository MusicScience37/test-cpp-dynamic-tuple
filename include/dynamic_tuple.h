#pragma once

#include <memory>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <variant>

#ifndef DYNAMIC_TUPLE_MAX_ELEMENTS
#define DYNAMIC_TUPLE_MAX_ELEMENTS (3)
#endif

namespace dynamic_tuple {

namespace impl {

/*!
 * \brief list of types
 *
 * \tparam Types types
 */
template <typename... Types>
struct type_list {};

/*!
 * \brief base class of storages of tuples
 *
 * \tparam VisitorType type of visitors of tuples
 * \tparam ElementTypeList list of types of elements in tuples
 */
template <typename VisitorType, typename ElementTypeList>
class tuple_storage_base;

/*!
 * \brief implementation of tuple_storage_base
 *
 * \tparam VisitorType type of visitors of tuples
 * \tparam ElementTypes types of elements in tuples
 */
template <typename VisitorType, typename... ElementTypes>
class tuple_storage_base<VisitorType, type_list<ElementTypes...>> {
public:
    //! type of this class
    using this_type =
        tuple_storage_base<VisitorType, type_list<ElementTypes...>>;

    //! type of visitors of tuples
    using visitor_type = VisitorType;

    //! list of types of elements in tuples
    using element_types = type_list<ElementTypes...>;

    //! type of variant objects of elements
    using variant_type = std::variant<ElementTypes...>;

    /*!
     * \brief call a visitor function object with the tuple this object has
     *
     * \param visitor visitor
     */
    virtual void visit(visitor_type& visitor) const = 0;

    /*!
     * \brief concatenate with an additional element
     *
     * \param element additional element
     * \return concatenated tuple
     */
    virtual std::shared_ptr<this_type> concatenate(
        const variant_type& element) const = 0;

    //! construct
    tuple_storage_base() = default;

    //! destruct
    virtual ~tuple_storage_base() = default;

    tuple_storage_base(const tuple_storage_base&) = delete;
    tuple_storage_base(tuple_storage_base&&) = delete;
    tuple_storage_base& operator=(const tuple_storage_base&) = delete;
    tuple_storage_base& operator=(tuple_storage_base&&) = delete;
};

/*!
 * \brief class of storages of tuples
 *
 * \tparam VisitorType type of visitors of tuples
 * \tparam StorableElementTypeList list of types of elements storable in tuples
 * \tparam TupleType type of tuples
 */
template <typename VisitorType, typename StorableElementTypeList,
    typename TupleType, typename = void>
class tuple_storage;

/*!
 * \brief implementation of tuple_storage
 *
 * \tparam VisitorType type of visitors of tuples
 * \tparam StorableElementTypes types of elements storable in tuples
 * \tparam TupleElementTypes types of elements actually stored in tuples
 */
template <typename VisitorType, typename... StorableElementTypes,
    typename... TupleElementTypes>
class tuple_storage<VisitorType, type_list<StorableElementTypes...>,
    std::tuple<TupleElementTypes...>,
    std::enable_if_t<(
        sizeof...(TupleElementTypes) <= DYNAMIC_TUPLE_MAX_ELEMENTS)>>
    : public tuple_storage_base<VisitorType,
          type_list<StorableElementTypes...>> {
public:
    //! type of base class
    using base_type =
        tuple_storage_base<VisitorType, type_list<StorableElementTypes...>>;

    //! type of visitors of tuples
    using visitor_type = typename base_type::visitor_type;

    //! list of types of elements in tuples
    using element_types = typename base_type::element_types;

    //! type of tuples
    using tuple_type = std::tuple<TupleElementTypes...>;

    //! type of variant objects of elements
    using variant_type = typename base_type::variant_type;

    /*!
     * \brief construct
     *
     * \param tuple tuple
     */
    explicit tuple_storage(const tuple_type& tuple) : tuple_(tuple) {}

    /*!
     * \brief call a visitor function object with the tuple this object has
     *
     * \param visitor visitor
     */
    void visit(visitor_type& visitor) const override { visitor(tuple_); }

    /*!
     * \brief concatenate with an additional element
     *
     * \param element additional element
     * \return concatenated tuple
     */
    std::shared_ptr<base_type> concatenate(
        const variant_type& element) const override {
        return std::visit(
            [this](const auto& elem) {
                using new_obj_type = tuple_storage<visitor_type, element_types,
                    std::tuple<TupleElementTypes...,
                        std::decay_t<decltype(elem)>>>;
                const auto ptr = std::make_shared<new_obj_type>(
                    std::tuple_cat(tuple_, std::tie(elem)));
                return static_cast<std::shared_ptr<base_type>>(ptr);
            },
            element);
    }

private:
    //! tuple
    tuple_type tuple_;
};

/*!
 * \brief implementation of tuple_storage for too many elements
 *
 * \tparam VisitorType type of visitors of tuples
 * \tparam StorableElementTypes types of elements storable in tuples
 * \tparam TupleElementTypes types of elements actually stored in tuples
 */
template <typename VisitorType, typename... StorableElementTypes,
    typename... TupleElementTypes>
class tuple_storage<VisitorType, type_list<StorableElementTypes...>,
    std::tuple<TupleElementTypes...>,
    std::enable_if_t<(
        sizeof...(TupleElementTypes) > DYNAMIC_TUPLE_MAX_ELEMENTS)>>
    : public tuple_storage_base<VisitorType,
          type_list<StorableElementTypes...>> {
public:
    //! type of base class
    using base_type =
        tuple_storage_base<VisitorType, type_list<StorableElementTypes...>>;

    //! type of visitors of tuples
    using visitor_type = typename base_type::visitor_type;

    //! type of tuples
    using tuple_type = std::tuple<TupleElementTypes...>;

    //! type of variant objects of elements
    using variant_type = typename base_type::variant_type;

    /*!
     * \brief construct
     */
    explicit tuple_storage(const tuple_type& /*tuple*/) {
        throw std::runtime_error("too many tuple elements");
    }

    /*!
     * \brief call a visitor function object with the tuple this object has
     */
    void visit(visitor_type& /*visitor*/) const override {}

    /*!
     * \brief concatenate with an additional element
     *
     * \return concatenated tuple
     */
    std::shared_ptr<base_type> concatenate(
        const variant_type& /*element*/) const override {
        return nullptr;
    }
};

}  // namespace impl

/*!
 * \brief class of dynamic tuple
 *
 * \tparam VisitorType type of visitors of tuples
 * \tparam ElementTypes types of elements storable in tuples
 */
template <typename VisitorType, typename... ElementTypes>
class dynamic_tuple {
public:
    //! type of visitors of tuples
    using visitor_type = VisitorType;

    //! list of types of elements in tuples
    using element_types = impl::type_list<ElementTypes...>;

    //! type of variant objects of elements
    using variant_type = std::variant<ElementTypes...>;

    //! construct empty tuple
    dynamic_tuple() = default;

    /*!
     * \brief add an element to the tuple
     *
     * \param element element
     */
    void push_back(const variant_type& element) {
        storage_ = storage_->concatenate(element);
    }

    /*!
     * \brief call a visitor function object with the tuple
     *
     * \param visitor visitor
     */
    void visit(visitor_type& visitor) const { storage_->visit(visitor); }

private:
    //! storage of tuples
    std::shared_ptr<impl::tuple_storage_base<visitor_type, element_types>>
        storage_{std::make_shared<
            impl::tuple_storage<visitor_type, element_types, std::tuple<>>>(
            std::tuple<>())};
};

}  // namespace dynamic_tuple
