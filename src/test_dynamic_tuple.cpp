#include <iostream>
#include <string>

#include <cxxabi.h>

#include "dynamic_tuple.h"

struct type_name_visitor {
    template <typename Tuple>
    void operator()(const Tuple& /*data*/) const {
        int status = 0;
        std::cout << abi::__cxa_demangle(typeid(Tuple).name(), 0, 0, &status)
                  << std::endl;
    }
};

int main(int argc, char** argv) {
    try {
        dynamic_tuple::dynamic_tuple<type_name_visitor, int, double,
            std::string>
            tuple;

        for (int i = 1; i < argc; ++i) {
            const std::string type = argv[i];
            if (type == "int") {
                tuple.push_back(int());
            } else if (type == "double") {
                tuple.push_back(double());
            } else if (type == "str") {
                tuple.push_back(std::string());
            } else {
                throw std::runtime_error("invalid type " + type);
            }
        }

        type_name_visitor visitor;
        tuple.visit(visitor);

        return 0;
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
